use anyhow::Result;
use sqlite::Connection;
use std::{path::{PathBuf, Path}, env, fs::{self, canonicalize}};
#[allow(unused_imports)]
use log::{debug, error, log_enabled, info, Level};
use chrono::prelude::*;
use clap::{Parser, Subcommand};
use git_repository as git;

const NAME: &str = env!("CARGO_PKG_NAME");
const TABLE_NAME: &str = "repos";

#[derive(Parser)]
#[clap(author, version, about, long_about = "TODO: add about clause")]
struct Args {
    #[clap(short, long, value_parser)]
    database: Option<String>,

    #[clap(short, long, action)]
    quiet: bool,

    #[clap(short, long, value_parser)]
    repo_root: Option<String>,

    #[clap(subcommand)]
    command: Option<Commands>, 
}

#[derive(Subcommand)]
enum Commands {
    List,
    GC,
    Update
}

fn timestamp() -> String {
    let timestamp: DateTime<Local> = Local::now();
    timestamp.to_string()
}

fn repo_is_registered(conn: &mut Connection, repo_root: &Path) -> Result<bool> {
    let mut statement = conn.prepare(
            format!("SELECT path FROM {} WHERE path=?", TABLE_NAME)
        )?
        .bind(1, repo_root.to_str())?;
    Ok(statement.next()? != sqlite::State::Done)
}

fn register(conn: &mut Connection, repo_root: PathBuf) -> Result<()> {  
    let timestamp = timestamp();
    debug!("Registering {} with timestamp {}", repo_root.display(), timestamp);
    let mut statement = conn.prepare(
            format!("INSERT INTO {} VALUES (?, ?)", TABLE_NAME)
        )?
        .bind(1, repo_root.as_path().to_str())?
        .bind(2, timestamp.as_str())?;

    // This executes the statement?
    while let sqlite::State::Row = statement.next()? {}

    Ok(())
}

fn list(conn: &mut Connection) -> Result<()> {
    let home = env!("HOME");
    debug!("Listing table contents");
    let mut cursor = conn.prepare(
            format!("SELECT * FROM {}", TABLE_NAME)
        )?
        .into_cursor();
    println!("{}", TABLE_NAME);
    println!("=============");
    while let Some(Ok(row)) = cursor.next() {
        let formatted_path = row.get::<String, _>(0).replace(home, "~");
        println!("* {} ({})", formatted_path, row.get::<String, _>(1));
    }
    Ok(())
}

fn garbage_collect(conn: &mut Connection) -> Result<()> {
    debug!("Running garbage collection");
    // TODO should we also check for duplicate entries?
    let mut cursor = conn.prepare(
            format!("SELECT path FROM {}", TABLE_NAME)
        )?
        .into_cursor();
    while let Some(Ok(row)) = cursor.next() {
        let key = row.get::<String, _>(0);
        let path = PathBuf::from(&key);
        // Canonicalizing the path fails if it does not exist
        // Use this to check if the path exists
        if path.canonicalize().is_ok() {
            debug!("{} still exists", key);
            continue;
        }
        // This path no longer exists on filesystem. Remove from database
        debug!("Removing {} from database", path.display());
        let mut statement = conn.prepare(
                format!("DELETE FROM {} WHERE path = ?", TABLE_NAME)
            )?
            // Use the path key that came from the database so we don't change the formatting
            .bind(1, key.as_str())?;

        // This executes the statement?
        while let sqlite::State::Row = statement.next()? {} 
    } 
    Ok(())
}

fn update(conn: &mut Connection, repo_root: PathBuf) -> Result<()> {
    let timestamp = timestamp();
    debug!("Updating {} with timestamp {}", repo_root.display(), timestamp);

    let mut statement = conn.prepare(
            format!("UPDATE {} SET timestamp = ? WHERE path = ?", TABLE_NAME)
        )?
        .bind(1, timestamp.as_str())?
        .bind(2, repo_root.as_path().to_str())?;

    // This executes the statement?
    while let sqlite::State::Row = statement.next()? {}

    Ok(())
}

fn find_git_root_dir() -> Result<PathBuf> {
    let repo = git::discover(".")?.apply_environment();
    if let Some(git_root_dir) = repo.path().parent() {
        return Ok(PathBuf::from(git_root_dir));
    }
    anyhow::bail!("");
}

fn find_cache_dir() -> Result<String> {
    if let Ok(config_dir) = env::var("XDG_CACHE_HOME") {
        return Ok(config_dir)
    }
    if let Ok(home) = env::var("HOME") {
        return Ok(format!("{}/.cache", home));
    }
    anyhow::bail!("Unable to find cache dir");
}

fn main() -> Result<()> { 
    env_logger::init();

    // TODO: Add debug or verbose options
    let args = Args::parse();
    let quiet = args.quiet;

    let database: PathBuf;
    if let Some(input) = args.database {
        database = PathBuf::from(input);
    }
    else {
        database = [find_cache_dir()?.as_str(), NAME, "database"].iter().collect(); 
    }

    if let Some(parent_dir) = database.parent() { 
       fs::create_dir_all(parent_dir)?;
    }     
    debug!("Database: {}", database.display());


    // TODO: add a column in the database to note a timestamp when we first cloned the repo
    // TODO: make a web request to github/lab to get the repo's description. Save that description
    // as a column in the database. Update once every now and then?
    // TODO: detect multiple clones of the same repo

    let mut conn = sqlite::open(database)?;
    conn.execute(
        format!("CREATE TABLE IF NOT EXISTS {}(
                path TEXT,
                timestamp TEXT
                )", TABLE_NAME)
        )?;

    match &args.command {
        Some(command) => {
            match command { 
                Commands::List => {
                   list(&mut conn)?;
                },
                Commands::GC => {
                    garbage_collect(&mut conn)?;
                    if ! quiet {
                        list(&mut conn)?;
                    } 
                },
                Commands::Update => {
                    // Handle commands that work with a specifc repo
                    // TODO: refactor so that only this subcommand has the repo_root option
                    let repo_root: PathBuf;
                    if let Some(value) = args.repo_root {
                        repo_root = PathBuf::from(value); 
                    } 
                    else if let Ok(git_dir) = find_git_root_dir() {
                        // Try to find the git dir encompasing our working directory
                        repo_root = git_dir;
                    }
                    else {
                        anyhow::bail!("Unable to find git dir");
                    }

                    let repo_root = canonicalize(repo_root)?;
                    debug!("Repo root: {}", repo_root.display());

                    if repo_is_registered(&mut conn, &repo_root)? {
                        update(&mut conn, repo_root)?;
                    }
                    else {
                        register(&mut conn, repo_root)?;
                    }
                    if ! quiet {
                        list(&mut conn)?;
                    }
               },
            }
        },
        None => {
            // Default to listing
            list(&mut conn)?;
        }
    }
    Ok(())
}
