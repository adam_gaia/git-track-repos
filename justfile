set positional-arguments

export RUST_LOG := "debug"
export RUST_BACKTRACE := "1"

default: run

build:
  cargo lbuild

test:
  cargo ltest

run *args='list':
  cargo lrun -- "$@"

